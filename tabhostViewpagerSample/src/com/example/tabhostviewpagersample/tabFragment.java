package com.example.tabhostviewpagersample;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

abstract public class tabFragment extends Fragment {
	private TabHost mTabHost;
	private ViewPager mPager;
	private HashMap<String, Fragment> fragmentMap = new HashMap<String, Fragment>();
	private ArrayList<Fragment> list = new ArrayList<Fragment>();
	private TabFragmentPagerAdapter mAdapter;

	abstract public void addFragment();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mytab, container,
				false);
		mPager = (ViewPager) view.findViewById(R.id.viewpager);
		mTabHost = (TabHost) view.findViewById(R.id.tabhost);
		mTabHost.setup();
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
					@Override
					public void onTabChanged(String tabId) {
						mPager.setCurrentItem(mPager.getAdapter()
								.getItemPosition(fragmentMap.get(tabId)));
					}
				});

			}
		});

		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				mTabHost.setCurrentTab(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (list.size() == 0) {
			addFragment();
		}

		if (mAdapter == null) {
			mAdapter = new TabFragmentPagerAdapter(getChildFragmentManager());
			mAdapter.setList(list);
			mPager.setAdapter(mAdapter);
		}

	}

	public void addFragmentTab(int titleResId, int tabViewResId,
			Fragment fragment, String tag) {
		list.add(fragment);
		fragmentMap.put(tag, fragment);
		TextView tabView = (TextView) LayoutInflater.from(getActivity())
				.inflate(tabViewResId, null);
		tabView.setText(titleResId);

		TabSpec tabSpec = mTabHost.newTabSpec(tag).setIndicator(tabView);
		tabSpec.setContent(new TabFactory(getActivity()));
		mTabHost.addTab(tabSpec);

	}

	class TabFactory implements TabContentFactory {

		private final Context mContext;

		public TabFactory(Context context) {
			mContext = context;
		}

		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}

	}

	class TabFragmentPagerAdapter extends FragmentPagerAdapter {
		private ArrayList<Fragment> fragments;

		public TabFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		public void setList(ArrayList<Fragment> fragments) {
			this.fragments = fragments;
		}

		@Override
		public Fragment getItem(int arg0) {
			return fragments.get(arg0);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}

		@Override
		public int getItemPosition(Object object) {
			int index = -1;
			for (int i = 0; i < fragments.size(); i++) {
				if (fragments.get(i).equals(object)) {
					index = i;
					break;
				}
			}

			return index;
		}
	}
}
