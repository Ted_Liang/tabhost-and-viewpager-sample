package com.example.tabhostviewpagersample;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.widget.FrameLayout;

public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		android.support.v4.app.FragmentManager  fm = getSupportFragmentManager();  
        FragmentTransaction ft = fm.beginTransaction();  
        ft.replace(R.id.simple_fragment, new customTabFragment());  
        ft.addToBackStack(null);  
        ft.commit();   
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
